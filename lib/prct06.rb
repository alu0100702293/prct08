require "prct06/version"

module Prct06

	require "prct06/referencia.rb"
	require "prct06/bibliografia.rb"
	require "prct06/ref_articulo.rb"
	require "prct06/ref_libro.rb"
	
end
